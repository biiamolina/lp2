<?php

/**
 * Esta classe representa um cart�o do framework MDBoostrap.
 * 
 * @link https://mdbootstrap.com/components/cards/
 * @author Reginaldo do Prado
 * @version 1.0
 * @since 20/03/2017
 * @package component
 * @aula Introdu��o � POO
 */
class Card{
	/** R�tulo do bot�o */
	private $btnLabel;
	
	/** Largura do cart�o */
	private $width;
	
	/** Dist�ncia aparente entre o cart�o e a p�gina */
	private $depth;
	
	/** O URL da imagem que aparece no cart�o */
	private $image;
	
	/** O t�tulo do cart�o */
	private $title;
	
	/** O texto do cart�o */
	private $text;
	
	
	function __construct($title, $text) {
		$this->title = $title;
		$this->text = $text;
	}
	
	/**
	 * Define o endere�o da imagem a ser carregada no cart�o
	 * @param string $url
	 */
	public function setImageUrl($url) {
		$this->image = $url;
	}
	
	/**
	 * Define o r�tulo do bot�o do card
	 * @param string $label
	 */
	public function setButtonLabel($label) {
		$this->btnLabel = $label;
	}
	
	private function getImage(){
		$html = '<img class="img-fluid" src="https://mdbootstrap.com/
				img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" 
				alt="Card image cap">';
		return $html;
	}
	
	private function getBlock(){
		$html = '<div class="card-block">
			        <h4 class="card-title">'.$this->title.'</h4>
			        <p class="card-text">'.$this->text.'.</p>
			        <a href="#" class="btn btn-primary">'.$this->btnLabel.'</a>
			    </div>';
		return $html;
	}
	
	public function getHTML() {
		$html = '<div class="card">';
		$html .= $this->getImage();
		$html .= $this->getBlock();
		$html .= '</div>';
		return $html;
	}
	
}



