<?php

/**
 * Classe que representa o endere�o de um usu�rio.
 *
 */
class Endereco {
	private $logradouro;
	private $numero;
	private $bairro;
		
	// construtor padr�o do php
	function __construct($logradouro, $numero, $bairro) {
		
		// defini��o de valor para membros da classe
		$this->logradouro = $logradouro;
		$this->numero = $numero;
		$this->bairro = $bairro;
	}
	
	// m�todos acessores
	
	/**
	 * Retorna o logradouro do endere�o
	 * @return string
	 */
	public function getLogradouro(){
		return $this->logradouro;
	}
	
	/**
	 * Retorna o numero de um endere�o
	 * @return int
	 */
	public function getNumero() {
		return $this->numero;
	}
	
	/**
	 * Retorna o bairro de um endere�o
	 * @return string
	 */
	public function getBairro() {
		return $this->bairro
		;
	}
}