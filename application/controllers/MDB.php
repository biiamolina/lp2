<?php

class MDB extends CI_Controller{
	
	// controlador � um gerador de p�ginas sua finalidade
	// � controlar o fluxo de exibi��o das p�ginas
	
	function panel(){
		$this->load->model('MDBModel', 'mdb');
		$data['card'] = $this->mdb->getCard();
		$html = $this->load->view('component/card', $data, true);
		$this->show($html);
	}

	function show($content) {
		$html  = $this->load->view('common/header', null, true);
		$html .= $this->load->view('common/navbar', null, true);
		$html .= $content;
		$html .= $this->load->view('common/footer', null, true);
		echo $html;
	}
}