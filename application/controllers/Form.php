<?php
// linha de seguran��o para evitar acesso direto a este script
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller{
	
	function index() {	
		
		// carregue o arquivo ContactFormModel e coloque na vari�ve 'model'
		$this->load->model('ContactFormModel', 'model');
		
		// carregar a view do carousel
		$info['file'] = array(
			array('img'=>15, 'titulo'=>'', 'descricao'=>''),
			array('img'=>16),
			array('img'=>17)
		);
		$content = $this->load->view('form/carousel', $info, true);
		
		// carregar a view do contact form
		$data = $this->model->getFormData();
		$content .= $this->load->view('form/contactform', $data, true);
		$this->show($content);
	}
	
	function show($content, $use_navbar = true){
		$html = $this->load->view('common/header', null, true);
		$html .= $content;
		$html .= $this->load->view('common/footer', null, true);
		echo $html;
	}
}