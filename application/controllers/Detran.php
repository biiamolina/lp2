<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// a cria��o de um controlador pode ser entendida
// como a cria��o de uma se��o no seu site...
// pense em cada fun��o como uma subse��o

// um controlador � um gerador de p�ginas
class Detran extends CI_Controller {
	
	function index() {
		$html = $this->load->view('detran/intro', null, true);
		$this->show($html, false);
	}
	
	function multas() {
		$html = $this->load->view('detran/form_multas', null, true);
		$html .= $this->load->view('detran/exibe_multas', null, true);
		$this->show($html);
	}
	
	function habilitacao() {
		$this->show('');
	}
	
	function cfc(){
		$this->show('');
	}
	
	function recursos() {
		$this->show('');
	}

	// copiar e colar n�o � coisa de gente decente
	function show($content, $use_navbar = true){
		$html = $this->load->view('common/header', null, true);
		
		if($use_navbar)
			$html .= $this->load->view('common/navbar', null, true);
		
		$html .= $content; 
		$html .= $this->load->view('common/footer', null, true);
		echo $html;
	}
}