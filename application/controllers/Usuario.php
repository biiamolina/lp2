<?php

class Usuario extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('UsuarioModel', 'model');
	}
	
	function index() {
		$data['table_body'] = $this->model->getTableBody();
		$data['table_header'] = $this->model->getTableHeader();		
		$html = $this->load->view('usuario/usuario_list', $data, true);
		$this->show($html);
	}
	
	
	function insere() {
		$this->model->insere();
		redirect('usuario');
	}
	
	
	function criar() {
		$label = $this->model->getLabels();
		$html = $this->load->view('usuario/usuario_form', $label, true);
		$this->show($html);
	}
	
	function remover($id) {
		$this->model->remover($id);
		redirect('usuario');
	}

	function editar($id) {
		echo "editar usuario numero $id";
	}
	
	function endereco() {
		$data = $this->model->getEndereco();		
		$html = $this->load->view('usuario/endereco', $data, true);
		$this->show($html);
	}
	
	function show($content) {
		$html  = $this->load->view('common/header', null, true);
		$html .= $this->load->view('common/navbar', null, true);
		$html .= $content;
		$html .= $this->load->view('common/footer', null, true);
		echo $html;
	}
}