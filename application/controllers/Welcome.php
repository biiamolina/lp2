<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	/**
	 * Cada fun��o em um controlador DEVE gerar uma p�gina diferente
	 * 
	 */
	public function nome($name, $idade){
		echo "Ol&aacute; $name... voce tem $idade anos?";
	}
	
	public function produto($setor, $id){
		echo "Produto num. $id do setor $setor carregado com sucesso!";
	}
}
