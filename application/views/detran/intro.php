
<!--Navigation & Intro-->
<header>

    <!--Navbar-->
    <nav class="navbar navbar-toggleable-md navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand waves-effect waves-light" href="http://mdbootstrap.com/material-design-for-bootstrap/" target="_blank">MDB</a>
            <div class="collapse navbar-collapse" id="navbarNav1">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('detran') ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('detran/cfc') ?>">C.F.C</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('detran/recursos') ?>">Recursos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('detran/multas') ?>">Multas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('detran/habilitacao') ?>">Habilita&ccedil;&atilde;o</a>
                    </li>
                </ul>
                <!--Navbar icons-->
                <ul class="nav navbar-nav nav-flex-icons ml-auto">
                    <li class="nav-item">
                        <a class="nav-link"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"><i class="fa fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--/.Navbar-->

    <!--Mask-->
    <div class="view hm-black-strong">
        <div class="full-bg-img flex-center">
            <div class="container">
                <div class="row" id="home">

                    <!--First column-->
                    <div class="col-lg-6">
                        <div class="description">
                            <h2 class="h2-responsive wow fadeInLeft">Make purchases with our app </h2>
                            <hr class="hr-dark">
                            <p class="wow fadeInLeft" data-wow-delay="0.4s">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem repellendus quasi fuga nesciunt dolorum nulla magnam veniam sapiente, fugiat! Commodi sequi non animi ea dolor molestiae, quisquam iste, maiores. Nulla.</p>
                            <br>
                            <a class="btn btn-white-outline btn-lg wow fadeInLeft" data-wow-delay="0.7s">Learn more</a>
                            <a class="btn btn-white-outline btn-lg wow fadeInLeft" data-wow-delay="0.7s">Download <i class="fa fa-android left right" aria-hidden="true"></i>
                        <i class="fa fa-apple left" aria-hidden="true"></i>
                        <i class="fa fa-windows" aria-hidden="true"></i></a>

                        </div>
                    </div>
                    <!--/.First column-->

                    <!--Second column-->
                    <div class="col-lg-4 offset-lg-1 flex-center">
                        <!--Form-->
                        <img src="https://mdbootstrap.com/img/Mockups/Transparent/Small/iphone10.png" alt="" class="img-fluid wow fadeInRight" id="app-mockup">
                        <!--/.Form-->
                    </div>
                    <!--/Second column-->
                </div>
            </div>
        </div>
    </div>
    <!--/.Mask-->

</header>
<!--/Navigation & Intro-->
