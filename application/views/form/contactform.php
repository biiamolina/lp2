
<div class="container col-md-6" >
<!--Naked Form-->
<div class="card-block">

    <!--Header-->
    <div class="text-center">
        <h3><i class="fa fa-envelope"></i><?= $form_title ?></h3>
        <hr class="mt-2 mb-2">
    </div>

    <!--Body-->
    <p><?= $intro_msg ?></p>
    <br>

    <!--Body-->
    <div class="md-form">
        <i class="fa fa-user prefix"></i>
        <input type="text" id="form3" class="form-control">
        <label for="form3"><?= $your_name ?></label>
    </div>

    <div class="md-form">
        <i class="fa fa-envelope prefix"></i>
        <input type="text" id="form2" class="form-control">
        <label for="form2"><?= $your_email ?></label>
    </div>

    <div class="md-form">
        <i class="fa fa-tag prefix"></i>
        <input type="text" id="form32" class="form-control">
        <label for="form34"><?= $subject ?></label>
    </div>

    <div class="md-form">
        <i class="fa fa-pencil prefix"></i>
        <textarea type="text" id="form8" class="md-textarea"></textarea>
        <label for="form8"><?= $message ?></label>
    </div>

    <div class="text-center">
        <button class="btn btn-default"><?= $button_label ?></button>

        <div class="call">
            <br>
            <p><?= $alt_msg ?>
                <br>
                <span><i class="fa fa-phone"> </i></span> + 01 234 565 280</p>
        </div>
    </div>

</div>
<!--Naked Form-->
</div>