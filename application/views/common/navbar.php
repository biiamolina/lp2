<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">Detran</a>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('detran') ?>">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('detran/cfc') ?>">C.F.C.</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('detran/recursos') ?>">Recursos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('detran/multas') ?>">Multas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('detran/habilitacao') ?>">Habilita&ccedil;&atilde;o</a>
      </li>
    </ul>
  </div>
</nav>