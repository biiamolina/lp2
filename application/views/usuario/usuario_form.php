
<!--Form with header-->
<div class="card col-md-5 mt-3 mx-auto">
    <div class="card-block">

        <!--Header-->
        <div class="form-header blue">
            <h3><i class="fa fa-user"></i><?= $form_title ?></h3>
        </div>

        <!--Body-->
        <form action="insere" method="post">
        <div class="md-form">
            <i class="fa fa-user prefix"></i>
            <input type="text" name="input_name" class="form-control">
            <label for="form3"><?= $input_name ?></label>
        </div>
        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="text" name="input_sname" class="form-control">
            <label for="form2"><?= $input_sname ?></label>
        </div>

        <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input type="number" name="input_idade" class="form-control">
            <label for="form4"><?= $input_idade ?></label>
        </div>
        
        <fieldset class="form-group">
        	<i class="fa fa-intersex prefix"></i>
		    <input type="radio" name="input_sexo" value=0>
		    <label class="mr-5" for=""><?= $input_sex_f ?></label>
		    
		    <input type="radio" name="input_sexo" value=1>
		    <label for=""><?= $input_sex_m ?></label>
		</fieldset>

        <div class="text-center">
            <input type="submit" class="btn btn-primary"><hr>
        </div>
		</form>
    </div>
</div>
<!--/Form with header-->
