<?php

class ContactFormModel extends CI_Model {
	
	// sem isso o model n�o funciona
	function __construct() {
		parent::__construct();
	}
	
	function getFormData() {
		$data['form_title'] = "Fale Conosco";
		$data['intro_msg'] = 'Mande sua mensagem e entraremos em contato';
		$data['your_name'] = 'Nome';
		$data['your_email'] = 'E - Mail';
		$data['subject'] = 'Assunto';
		$data['message'] = 'Mensagem';
		$data['button_label'] = 'Enviar Mensagem';
		$data['alt_msg'] = 'Ou, se preferir, ligue para:';
		return $data;
	}
}