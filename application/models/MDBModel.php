<?php
include APPPATH.'libraries/Component/Card.php';

class MDBModel extends CI_Model{
	
	function __construct() {
		parent::__construct();
	}
	
	function getCard() {
		$text = 'As palavras-chave desta disciplina sao: 
				 modularizacao, organizacao e reutilizacao.';
		$card = new Card('LPII - 2017/1', $text);
		$card->setButtonLabel('IFSP - GRU');
		return $card->getHTML();
	}
	
}