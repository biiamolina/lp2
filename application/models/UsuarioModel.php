<?php
include APPPATH.'libraries/Endereco.php';

class UsuarioModel extends CI_Model {
	
	function insere() {
		$data['nome'] = $this->input->post('input_name');
		$data['snome'] = $this->input->post('input_sname');
		$data['idade'] = $this->input->post('input_idade');
		$data['sexo'] = $this->input->post('input_sexo');
		$this->db->insert('usuario', $data);
	}
	
	function getLabels() {
		$label['form_title'] = 'Cadastro de Usu&aacute;rio';
		$label['input_name'] = 'Nome';
		$label['input_sname'] = 'Sobrenome';
		$label['input_idade'] = 'Idade';
		$label['input_sex_m'] = 'Masculino';
		$label['input_sex_f'] = 'Feminino';
		return $label;
	}
	
	function getTableHeader() {
		return '<tr>
            <th></th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Idade</th>
            <th>Sexo</th>
			<th></th>
        </tr>';
	}
	
	function getTableBody() {
		$query = $this->db->get('usuario');
		$usuarios = $query->result_array();
		
		$html = ''; $cont = 1;
		foreach ($usuarios AS $usuario){
			$html .= '<tr>
            <th scope="row">'.$cont++.'</th>
            <td>'.$usuario['nome'].'</td>
            <td>'.$usuario['snome'].'</td>
            <td>'.$usuario['idade'].'</td>
			<td>'.$usuario['sexo'].'</td>
            <td>
                <a href="usuario/editar/'.$usuario['id'].'" class="teal-text"><i class="fa fa-pencil mr-3"></i></a>
                <a href="usuario/remover/'.$usuario['id'].'" class="red-text"><i class="fa fa-times"></i></a>
            </td>
        </tr>';
		}
		return $html;
	}
	
	
	function remover($id) {
		// codigo de verifica��o antes da remo��o...
		$this->db->delete('usuario', array('id' => $id));
	}
	
	
	function getEndereco() {
		$end = new Endereco('Av. Salgado Filho', 2345, 'Vila Rio de Janeiro');
		$data['rua'] = $end->getLogradouro();
		$data['num'] = $end->getNumero();
		$data['bairro'] = $end->getBairro();
		return $data;
	}
	
	
}